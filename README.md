# CSPARK-DEMO
## An introductory repository for using Git and GitLab
### Cambridge Spark September 2020

This repository is a demonstration tool for the "Introduction to Git" and "Advanced Git" units.

Description:
- `hello.py`: Takes a person's name and checks if the name is a palindrome

