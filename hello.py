import sys

def main(name_str):
	# Get first user argument
	print("Hello {}!".format(name_str))

	# Check if name is equal to name reversed
	is_palindrome = name_str == name_str[::-1]

	if is_palindrome:
		print("Your name is a palindrome!")
	else:
		print("Sorry, your name is not a palindrome...")

if __name__ == "__main__":
	name = sys.argv[1]
	main(name)
